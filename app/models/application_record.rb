class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  self.clear_cache!
end
