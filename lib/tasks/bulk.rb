module Bulk
  def self.defalut
    start_time = Time.now
    p "start insert"

    10000.times do |i|
      User.new(
        name: "user_#{i}"
      )
      User.save
    end

    p "処理時間 #{Time.now - start_time}s"  # "処理時間 113.238475153s"
  end

  def self.activerecord_import
    start_time = Time.now
    p "start insert"

    new_models = []
    10000.times do |i|
      new_models << User.new(
        name: "user_#{i}"
      )
    end
    User.import new_models

    p "処理時間 #{Time.now - start_time}s"  # "処理時間 2.181682652s"
  end
end