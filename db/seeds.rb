# frozen_string_literal: true

require 'faker'

class InitialDatabase
  # データをすべて削除してインクリメントをリセット
  def reset_models
    p 'reset databases'
    models.map(&:singularize).map(&:camelize).each do |model|
      eval("#{model}.delete_all")
      eval("#{model}.reset_pk_sequence")  # initilize primary_id
    end
  end

  private
  def models
    ActiveRecord::Base.connection.tables.reject do |model|
      # 元からあるテーブルは除外
      default_tables.include?(model)
    end
  end

  def default_tables
    ["schema_migrations", "ar_internal_metadata"]
  end
end

class SeedModels
  def create_users(num=1)
    p "create #{num} Users"
    num.times do
      User.create(
        name:         Faker::Name.name,
        age:          [*0...100].sample,
        phone_number: Faker::PhoneNumber.phone_number_with_country_code,
        mail_address: Faker::Internet.email,
        address:      Faker::Address.full_address,
      )
    end
  end
  
  def create_companies(num=1)
    p "create #{num} Companies"
    num.times do
      Company.create(
        name:         Faker::Company.name,
        suffix:       Faker::Company.suffix,
        industry:     Faker::Company.industry,
        logo:         Faker::Company.logo,
      )
    end
  end

  def associate_all(parent, child)
    child.all.each do |c|
      pa = parent.where(id: rand(1...parent.all.count)).first
      associate(pa, c)
    end
  end

  # parentにchildを紐付ける
  def associate(parent, child)
    p "associate #{child.class.name} with #{parent.class.name}"
    child.update_attributes("#{parent.class.name.downcase}_id".to_sym => parent.id)
  end
end

InitialDatabase.new.reset_models

sm = SeedModels.new
sm.create_users(100)
sm.create_companies(30)

# 関連付け
sm.associate_all(Company, User)

# accepts_nested_attributes_for
params = { machine: { 
           name: '紅蓮可翔式', 
           parts_attributes: [
             { name: '輻射波動砲' }, 
             { name: '飛翔滑走翼' }
           ]
         }}
Machine.create(params[:machine])
