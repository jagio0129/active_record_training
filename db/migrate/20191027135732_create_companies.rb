class CreateCompanies < ActiveRecord::Migration[5.1]
  def change
    create_table :companies do |t|
      t.string :name
      t.string :suffix
      t.string :industry
      t.binary :logo

      t.timestamps
    end
  end
end
